\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t
		% intro
		d1 d1

		% si sientes un murmullo...
		d1 d1 a1 a1 g1 g1 d1 a1

		% asi, abre el corazon...
		d1 d1 a1 a1 g1 g1 d1 a1

		% hay angeles volando...
		d1 d1 a1 a1 g1 g1 d1 a1
		d1 d1 a1 a1 g1 a1 d1 a1

		% hay angeles volando...
		d1 r1*6 a1
		d1 d1 a1 a1 g1 a1 d1 d1
	}
