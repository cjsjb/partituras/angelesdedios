\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key d \major

		R1*2  |
		fis 4 -\staccato r fis 8 fis fis fis  |
		fis 8 fis fis fis 4 e 8 d e ~  |
%% 5
		e 8 r e e e e e e ~  |
		e 8 e e e 4 d 8 cis d ~  |
		d 8 r d d d d d d ~  |
		d 8 d d d d e d 4  |
		fis 2 ( ~ fis 8 e d ) e ~  |
%% 10
		e 2 ~ e 8 r d' 4  |
		d' 4 r d' 8 d' d' d'  |
		d' 8 d' d' d' 4 cis' 8 b cis' ~  |
		cis' 8 r r cis' cis' cis' cis' cis' ~  |
		cis' 8 cis' cis' cis' cis' b a 4  |
%% 15
		b 4 -\staccato b 8 b b b b b ~  |
		b 8 b b b b cis' b 4  |
		d' 2 ( ~ d' 8 cis' b ) cis' ~  |
		cis' 2 ~ cis' 8 r r4  |
		R1*2  |
		cis' 8 cis' cis' 2. ~  |
		cis' 2 r4 fis (  |
		b 1 )  |
		r4 b 8 b b a g 4  |
%% 25
		a 2 ( ~ a 8 g fis ) a ~  |
		a 4. r8 r2  |
		r4 d' d' 2 ~  |
		d' 2. d' 8 cis' ~  |
		cis' 1  |
%% 30
		r4 cis' 8 cis' d' 4 cis'  |
		b 1  |
		r4 g 8 a 4. g 4  |
		fis 2.. r8  |
		R1*3  |
		cis' 8 cis' cis' 2. ~  |
		cis' 2 r4 fis (  |
		b 1 )  |
%% 40
		r4 b 8 b b a g 4  |
		a 2 ( ~ a 8 g fis ) a ~  |
		a 4. r8 r2  |
		r4 d' d' 2 ~  |
		d' 2. d' 8 cis' ~  |
%% 45
		cis' 1  |
		r4 cis' 8 cis' d' 4 cis'  |
		b 1  |
		r4 g 8 a 4. g 4  |
		fis 2.. r8  |
%% 50
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		%
		Si sien -- tes un mur -- mu -- llo
		muy cer -- ca de ti __
		es un án -- gel lle -- gan -- do
		pa -- ra re -- ci -- bir __
		to -- das tus -- o -- ra -- cio -- nes
		y lle -- var -- las al cie __ lo. __

		A -- sí, a -- "bre el" co -- ra -- zon
		y co -- mien -- "za a a" -- la -- bar __
		el go -- zo del cie -- lo to -- do
		so -- "bre el" al -- tar,
		hay un án -- gel lle -- gan -- "do, hay"
		ben -- di -- ción en sus ma __ nos. __

		¡Án -- ge -- les! __
		Ah. __
		"...to" -- das las di -- rec -- cio __ nes. __

		No sé... __
		"...ba" -- jó... __
		"...de án" -- ge -- les de Dios... __
		"...es" -- tá a -- quí.

		¡Án -- ge -- les! __
		Ah. __
		"...to" -- das las di -- rec -- cio __ nes. __

		No sé... __
		"...ba" -- jó... __
		"...de án" -- ge -- les de Dios... __
		"...es" -- tá a -- quí.
	}
>>
