\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key d \major

		R1*2  |
		fis' 4 -\staccato r fis' 8 fis' fis' fis'  |
		fis' 8 fis' fis' fis' 4 e' 8 d' e' ~  |
%% 5
		e' 8 r e' e' e' e' e' e' ~  |
		e' 8 e' e' e' 4 d' 8 cis' d' ~  |
		d' 8 r d' d' d' d' d' d' ~  |
		d' 8 d' d' d' d' e' d' 4  |
		d' 2 ( ~ d' 8 cis' b ) cis' ~  |
%% 10
		cis' 2 ~ cis' 8 r d' 4  |
		d' 4 r d' 8 d' d' d'  |
		d' 8 d' d' d' 4 cis' 8 b cis' ~  |
		cis' 8 r r cis' cis' cis' cis' cis' ~  |
		cis' 8 cis' cis' cis' cis' b a 4  |
%% 15
		b 4 -\staccato b 8 b b b b b ~  |
		b 8 b b b b cis' b 4  |
		d' 2 ( ~ d' 8 cis' b ) cis' ~  |
		cis' 2 ~ cis' 8 r r4  |
		d' 4 r d' 8 d' d' d'  |
%% 20
		d' 8 d' d' d' 4 d' 8 d' cis' ~  |
		cis' 4 r8 cis' cis' cis' cis' cis' ~  |
		cis' 8 cis' cis' d' 4 d' 8 cis' 4  |
		b 4 r8 b b b b b ~  |
		b 8 b b b b cis' b 4  |
%% 25
		d' 2 ( ~ d' 8 cis' b ) cis' ~  |
		cis' 4. r8 r4 d'  |
		d' 4 d' 8 d' d' d' d' d' ~  |
		d' 8 d' d' d' d' d' d' cis' ~  |
		cis' 4 r8 cis' cis' cis' cis' cis' ~  |
%% 30
		cis' 8 cis' cis' cis' d' 4 cis'  |
		b 4 r8 b b 8. b b 8  |
		a 4 a 8 a 4. a 4  |
		a 2.. r8  |
		R1  |
%% 35
		d' 4 r d' 8 d' d' d'  |
		d' 8 d' d' d' 4 d' 8 d' cis' ~  |
		cis' 4 r8 cis' cis' cis' cis' cis' ~  |
		cis' 8 cis' cis' d' 4 d' 8 cis' 4  |
		b 4 r8 b b b b b ~  |
%% 40
		b 8 b b b b cis' b 4  |
		d' 2 ( ~ d' 8 cis' b ) cis' ~  |
		cis' 8 r d' 4 d' d'  |
		d' 4 d' 8 d' d' d' d' d' ~  |
		d' 8 d' d' d' d' d' d' cis' ~  |
%% 45
		cis' 4 r8 cis' cis' cis' cis' cis' ~  |
		cis' 8 cis' cis' cis' d' 4 cis'  |
		b 4 r8 b b 8. b b 8  |
		a 4 a 8 a 4. a 4  |
		a 2.. r8  |
%% 50
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		%
		Si sien -- tes un mur -- mu -- llo
		muy cer -- ca de ti __
		es un án -- gel lle -- gan -- do
		pa -- ra re -- ci -- bir __
		to -- das tus -- o -- ra -- cio -- nes
		y lle -- var -- las al cie __ lo. __

		A -- sí, a -- "bre el" co -- ra -- zon
		y co -- mien -- "za a a" -- la -- bar __
		el go -- zo del cie -- lo to -- do
		so -- "bre el" al -- tar,
		hay un án -- gel lle -- gan -- "do, hay"
		ben -- di -- ción en sus ma __ nos. __

		Hay án -- ge -- les vo -- lan -- do en es -- te lu -- gar __
		en me -- dio del pue -- blo y jun -- "to al" al -- tar
		su -- bien -- "do y" ba -- jan -- "do en" to -- das las di -- rec -- cio __ nes. __

		No sé si "la i" -- gle -- sia su -- bió __ o si el cie -- lo ba -- jó, __
		sí sé "que es" -- tá lle -- no "de án" -- ge -- les de Dios
		por -- "que el" mis -- mo Dios es -- tá a -- quí.

		Hay án -- ge -- les vo -- lan -- do en es -- te lu -- gar __
		en me -- dio del pue -- blo y jun -- "to al" al -- tar
		su -- bien -- "do y" ba -- jan -- "do en" to -- das las di -- rec -- cio __ nes. __

		No, no, no sé si "la i" -- gle -- sia su -- bió __ o si el cie -- lo ba -- jó, __
		sí sé "que es" -- tá lle -- no "de án" -- ge -- les de Dios
		por -- "que el" mis -- mo Dios es -- tá a -- quí.
	}
>>
