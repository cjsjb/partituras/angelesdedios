\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key d \major

		R1*2  |
		fis' 4 -\staccato r fis' 8 fis' fis' fis'  |
		fis' 8 fis' fis' fis' 4 e' 8 d' e' ~  |
%% 5
		e' 8 r e' e' e' e' e' e' ~  |
		e' 8 e' e' e' 4 d' 8 cis' d' ~  |
		d' 8 r d' d' d' d' d' d' ~  |
		d' 8 d' d' d' d' e' d' 4  |
		fis' 2 ( ~ fis' 8 e' d' ) e' ~  |
%% 10
		e' 2 ~ e' 8 r fis' 4  |
		fis' 4 r fis' 8 fis' fis' fis'  |
		fis' 8 fis' fis' fis' 4 e' 8 d' e' ~  |
		e' 8 r r e' e' e' e' e' ~  |
		e' 8 e' e' e' e' d' cis' 4  |
%% 15
		d' 4 -\staccato d' 8 d' d' d' d' d' ~  |
		d' 8 d' d' d' d' e' d' 4  |
		fis' 2 ( ~ fis' 8 e' d' ) e' ~  |
		e' 2 ~ e' 8 r r4  |
		fis' 4 r fis' 8 fis' fis' fis'  |
%% 20
		fis' 8 fis' fis' fis' 4 g' 8 a' a' ~  |
		a' 4 r8 a' a' a' a' a' ~  |
		a' 8 a' a' b' 4 b' 8 a' 4  |
		g' 4 r8 g' g' g' g' g' ~  |
		g' 8 g' g' g' g' fis' e' 4  |
%% 25
		fis' 2 ( ~ fis' 8 e' d' ) e' ~  |
		e' 4. r8 r4 fis'  |
		fis' 4 fis' 8 fis' fis' fis' fis' fis' ~  |
		fis' 8 fis' fis' fis' fis' g' a' a' ~  |
		a' 4 r8 a' a' a' a' a' ~  |
%% 30
		a' 8 a' a' a' b' 4 a'  |
		g' 4 r8 g' g' 8. g' fis' 8  |
		e' 4 d' 8 fis' 4. e' 4  |
		d' 2.. r8  |
		R1  |
%% 35
		fis' 4 r fis' 8 fis' fis' fis'  |
		fis' 8 fis' fis' fis' 4 g' 8 a' a' ~  |
		a' 4 r8 a' a' a' a' a' ~  |
		a' 8 a' a' b' 4 b' 8 a' 4  |
		g' 4 r8 g' g' g' g' g' ~  |
%% 40
		g' 8 g' g' g' g' fis' e' 4  |
		fis' 2 ( ~ fis' 8 e' d' ) e' ~  |
		e' 8 r a' 4 a' g'  |
		fis' 4 fis' 8 fis' fis' fis' fis' fis' ~  |
		fis' 8 fis' fis' fis' fis' g' a' a' ~  |
%% 45
		a' 4 r8 a' a' a' a' a' ~  |
		a' 8 a' a' a' b' 4 a'  |
		g' 4 r8 g' g' 8. g' fis' 8  |
		e' 4 d' 8 fis' 4. e' 4  |
		d' 2.. r8  |
%% 50
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		%
		Si sien -- tes un mur -- mu -- llo
		muy cer -- ca de ti __
		es un án -- gel lle -- gan -- do
		pa -- ra re -- ci -- bir __
		to -- das tus -- o -- ra -- cio -- nes
		y lle -- var -- las al cie __ lo. __

		A -- sí, a -- "bre el" co -- ra -- zon
		y co -- mien -- "za a a" -- la -- bar __
		el go -- zo del cie -- lo to -- do
		so -- "bre el" al -- tar,
		hay un án -- gel lle -- gan -- "do, hay"
		ben -- di -- ción en sus ma __ nos. __

		Hay án -- ge -- les vo -- lan -- do en es -- te lu -- gar __
		en me -- dio del pue -- blo y jun -- "to al" al -- tar
		su -- bien -- "do y" ba -- jan -- "do en" to -- das las di -- rec -- cio __ nes. __

		No sé si "la i" -- gle -- sia su -- bió __ o si el cie -- lo ba -- jó, __
		sí sé "que es" -- tá lle -- no "de án" -- ge -- les de Dios
		por -- "que el" mis -- mo Dios es -- tá a -- quí.

		Hay án -- ge -- les vo -- lan -- do en es -- te lu -- gar __
		en me -- dio del pue -- blo y jun -- "to al" al -- tar
		su -- bien -- "do y" ba -- jan -- "do en" to -- das las di -- rec -- cio __ nes. __

		No, no, no sé si "la i" -- gle -- sia su -- bió __ o si el cie -- lo ba -- jó, __
		sí sé "que es" -- tá lle -- no "de án" -- ge -- les de Dios
		por -- "que el" mis -- mo Dios es -- tá a -- quí.
	}
>>
